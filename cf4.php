<?php

include 'setting.php';
include 'class-cloudflare.php';

date_default_timezone_set('Asia/Tokyo');
$ipcmd="/sbin/ip";
$type = "A";
$defaultttl = 120;

$flag=0;

foreach ($cf4_domains as $fqdn => $device) {
  $fqdn = strtolower($fqdn);
  foreach ($cf_domain_account as $cfdom => $cfmail){
    if (strstr($fqdn, $cfdom)) {
      $zone = $cfdom;
      $hostname = rtrim(str_replace($cfdom,'',$fqdn),'.');
      $email = $cfmail;
    }
  }
  $text = explode("@", $device);
  if (isset($text[1])){
    $ttl = intval($text[1]);
  }else{
    $ttl = $defaultttl;
  }
  $devs    = explode(",", $text[0]);
  $resolvs = dns_get_record ( $fqdn , DNS_A);
  $res_arr = array();
  foreach ($resolvs as $val) {array_push($res_arr, $val["ip"]);}
  $dev_arr = array();
  foreach ($devs as $dev) {
    if(preg_match('/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/', $dev)){
      $cmd = "echo $dev";
    }elseif (preg_match("/^(ssh|echo|curl|wget|cat)/",$dev)){
      $cmd = $dev;
    }else{
      $cmd = $ipcmd . " -f inet -o addr show $dev 2>/dev/null|cut -d' ' -f 7 | cut -d/ -f 1|head -c -1";
    }
    $ipz = rtrim(shell_exec($cmd));
    if ($ipz){$dev_arr=array_merge($dev_arr,explode("\n",$ipz));}
  }
  foreach ($dev_arr as &$v) { $v=inet_ntop(inet_pton($v));}
  foreach ($res_arr as &$v) { $v=inet_ntop(inet_pton($v));}
  $cf = new cloudflare_api($email,$cf_api_key[$email]);
  if (!array_equal_set( $res_arr, $dev_arr ) ) {
    $identifier = $cf->identifier($zone);
    $dns_records = $cf->get_dns_record($identifier,$type,$fqdn);
    foreach ($dns_records->result as $dns_record) {
      $cf->delete_dns_record($identifier,$dns_record->id);
    }
    foreach ($dev_arr as $ip) {
        $result = $cf->create_dns_record($identifier,$type,$fqdn,$ip,$ttl);
        if ($result->success){
          print "[".date("Y-m-d H:i:s")."]"."\t".str_pad($hostname.".".$zone, 25).str_pad($type, 6) .str_pad($ip, 40)."Set Succeed" . "\n";
        }else{
	  var_dump($result);
          print "[".date("Y-m-d H:i:s")."]"."\t".str_pad($hostname.".".$zone, 25).str_pad($type, 6) .str_pad($ip, 40)."Failed" . "\n";
        }
    }
  }else{
    foreach ($dev_arr as $ip) {
      print "[".date("Y-m-d H:i:s")."]"."\t".str_pad($hostname.".".$zone, 25).str_pad($type, 6) .str_pad($ip, 40)."OK\n";
    }
  }

}

function array_equal_set( $a, $b )
{
  $diff_a_to_b = array_diff($a, $b);
  $diff_b_to_a = array_diff($b, $a);
  return empty($diff_a_to_b) && empty($diff_b_to_a);
}

?>
